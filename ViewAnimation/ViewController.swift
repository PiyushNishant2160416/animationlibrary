//
//  ViewController.swift
//  ViewAnimation
//
//  Created by Piyush on 06/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var myview: UIView!

    @IBOutlet weak var animateview: AnimatableCustomView!
    
    
    @IBAction func buttonIsTapped(sender: UIButton) {
      // animateview.animate()
        let layeranimationObject = LayerAnimation(view: self.myview)
        //layeranimationObject.fading(fadingeffect: AnimationType.FadeOut)
        layeranimationObject.morphingEffect(AnimationType.Morph)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

