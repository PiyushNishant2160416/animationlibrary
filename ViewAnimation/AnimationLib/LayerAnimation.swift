import Foundation
import UIKit
public class LayerAnimation : NSObject{
    private unowned let viewLayer : CALayer
    
    
    //Screen Width and Height
    private let screenSize: CGRect = UIScreen.mainScreen().bounds
    private let screenWidth:CGFloat
    private let screenHeight :CGFloat
    private let viewObject : UIView
    
    //Constructor
    init(view:UIView) {
        viewObject = view
        // affineTransform = viewObject.transform
        self.viewLayer = view.layer
        self.screenWidth = self.screenSize.width
        self.screenHeight = self.screenSize.height
        super.init()
    }
    
    public var isautoStart : Bool = false
    public var isautoHide : Bool = false
    public var isremovedFromSuperView : Bool  = false
    public var animationType:AnimationType = AnimationType.FadeIn
    public var force :CGFloat = 1
    public var animationDelay : CGFloat = 0
    public var animationDuration : CGFloat = 0.5
    public var animationVelocity : CGFloat = 0.7
    public var animationDampingFactor : CGFloat = 0.7
    public var animationRepeatCount : Float = 1
    public var xcordinate : CGFloat = 0.0
    public var ycordinate :CGFloat = 0.0
    public var scalingfactorX : CGFloat  = 1.0
    public var scalingfactorY : CGFloat = 1.0
    public var rotationAngle : CGFloat = 0
    public var opacity : CGFloat = 1
    public var animationCurve : AnimationCurve = AnimationCurve.EaseInOut
    
    
    private var animateFrom : Bool = true
    private var affineTransform : CGAffineTransform{
        get{
            return self.viewObject.transform
        }
        set{
            self.viewObject.transform = newValue
        }
    }
    
    //MARK:Fading Effect
    /**
     Perform Fading Effect
     - parameter fadingeffect : AnimationType reference
     - returns : No value is return
     */
    public func fading(fadingeffect fadingeffect:AnimationType){
        animationType = fadingeffect
        var fromvalue:CGFloat = 0
        var tovalue:CGFloat = 0
        switch fadingeffect {
        case .FadeInOut:
            fromvalue = 1
            tovalue = 0
            fadoutinOrfadeInout(fromValue: fromvalue, toValue: tovalue)
        case .FadeOutIn :
            fromvalue = 0
            tovalue = 1
            fadoutinOrfadeInout(fromValue: fromvalue, toValue: tovalue)
        case .FadeIn :
            self.opacity = 1
        case .FadeInLeft :
            opacity = 0
            xcordinate = 300*force
        case .FadeInRight :
            opacity = 0
            xcordinate = -300*force
        case .FadeInDown :
            opacity = 0
            ycordinate = -300*force
        case .FadeInUp :
            opacity = 0
            ycordinate = 300*force
        case .FadeOut :
            self.opacity = 0
            self.animateFrom = false
            
        default:
            break
        }
        
        performAnimation({})
    }
    
    //MARK:Slide effect
    /**
     Perform Slide effect on Layer Basis
     - parameter slide,slidex,slidey : slide is a AnimationType reference,slidex is cgfloat reference,slidey is cgfloat reference
     - returns : No value is return
     */
    public func slideEffect(slide slide:AnimationType, slideX:CGFloat = 300,slideY:CGFloat = 300){
        animationType = slide
        animateFrom = true
        
        switch slide {
        case .SlideLeft:
            xcordinate = slideX*force
        case .SlideRight :
            xcordinate = -slideX*force
        case .SlideUp :
            ycordinate = slideY*force
        case .SlideDown :
            ycordinate = -slideY*force
        default:
            break
        }
        
        performAnimation({})
        
    }
    //MARK:Squeeze Effect
    /**
     Squeeze View Layer
     - parameter squeezex,squeezey,squeezedirection,scallingfactorx,scallingfactory : squeezex is CGFloat reference,squeezey is CGFloat,squeezedirection is ANimation Type,scallingfactorx is CGFloat reference,scallingfactory is CGFloat reference
     - returns : No value is return
    */
    public func squeezeEffect(squeeze squeezex:CGFloat = 300,squeezey:CGFloat = 300,squeezedirection:AnimationType,scallingfactorx:CGFloat = 3,scallingfactory:CGFloat = 3){
        switch squeezedirection {
        case .SqueezeLeft:
            xcordinate = squeezex
            self.scalingfactorX = scallingfactorx*force
        case .SqueezeRight:
            xcordinate = -squeezex
            self.scalingfactorX = scallingfactorx*force
        case .SqueezeUp :
            ycordinate = squeezey
            self.scalingfactorY = scallingfactory*force
        case .SqueezeDown :
            ycordinate = -squeezey
            self.scalingfactorY = scallingfactory*force
        default:
            break
        }
        performAnimation({})
    }
    
    //MARK:Zoom Effect
    /**
     Perform zoom effect on view layer basis
     - parameter scalingfactorx,scalingfactory,zoomType : scalling factor decide to which extent zoom effect is performed
     - returns : No value is return
 
    */
    public func zoomEffect(scalingfactorx scalingfactorx:CGFloat = 2,scalingfactory:CGFloat = 2,zoomType:AnimationType){
        switch zoomType {
        case .ZoomIn:
            opacity = 0
            scalingfactorY = scalingfactory*force
            scalingfactorX = scalingfactorx*force
        case .ZoomOut :
            opacity = 0
            scalingfactorY = 1/2*force
            scalingfactorX = 1/2*force
        default:
            break
        }
        performAnimation({})
        
    }
    
    //Fall Effect
    /**
     Perform the falling effect on view 
     - parameter Ycordinate : CGfloat type which decide upto what distance it falls
     - returns : No value is return
    */
    public func fall(Ycordinate Ycordinate:CGFloat = 300){
        self.animateFrom = false
        self.ycordinate = Ycordinate
        performAnimation({})
        
    }
    //MARK:Shake
    /**
     Perform Shaking effect in Horizontal and Vertical Direction
     - parameter animationDuration,repeatation,shakeDirection : parameter decide shaking effects
     - returns : No value is return
    */
    public func shake(animationDuration animationDuration:CFTimeInterval = 0.07,repeatation:Float = 2,shakeDirection:AnimationType,shakingDistance:CGFloat = 10){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = animationDuration
        animation.repeatCount = repeatation
        animation.autoreverses = true
        
        switch shakeDirection{
        case AnimationType.ShakeHorizontal :
            animation.fromValue = NSValue(CGPoint: CGPointMake(viewObject.center.x - shakingDistance, viewObject.center.y))
            animation.toValue = NSValue(CGPoint: CGPointMake(viewObject.center.x + shakingDistance, viewObject.center.y))
        case AnimationType.ShakeVertical :
            animation.fromValue = NSValue(CGPoint: CGPointMake(viewObject.center.x , viewObject.center.y - shakingDistance))
            animation.toValue = NSValue(CGPoint: CGPointMake(viewObject.center.x , viewObject.center.y + shakingDistance))
        default:
            animation.fromValue = NSValue(CGPoint: CGPointMake(viewObject.center.x - shakingDistance, viewObject.center.y))
            animation.toValue = NSValue(CGPoint: CGPointMake(viewObject.center.x + shakingDistance, viewObject.center.y))
        }
        
        viewObject.layer.addAnimation(animation, forKey: "position")
         performAnimation({})
    }
    //MARK:POP effect
    /**
     Perform pop Effect
     - parameter force,animationRepeatcount,animationDelay : Parameter decide pop efect
     - returns : No value is return
    */
    public func pop(force force:CGFloat = 1,animationRepeatcount:Float = 1,animationDelay:CGFloat = 0.5){
        let animation = CAKeyframeAnimation()
        animation.keyPath = "transform.scale"
        animation.values = [0, 0.2*force, -0.2*force, 0.2*force, 0]
        animation.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
        //animation.timingFunction = getTimingFunction(curve)
        animation.duration = CFTimeInterval(animationDuration)
        animation.additive = true
        animation.repeatCount = animationRepeatcount
        animation.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
        self.viewLayer.addAnimation(animation, forKey: "pop")
         performAnimation({})

    }
    //MARK:Wooble Effect
    /**
     Perform Wooble Effect and wooble effect with movement
     - parameter iswooblewithmove,woobleDirection,force,animationDuration,animationDelay : Parameter decide wooble efect parameters has default values
     - returns : No value is return
     */
    public func woobleEffect(iswooblewithmove:Bool = false,woobleDirection:AnimationType = AnimationType.WoobleHorizontal,force:CGFloat = 1,animationDuration:CGFloat = 0.5,animationDelay:CGFloat = 0){
        let basicAnimation = CAKeyframeAnimation(keyPath: "transform.rotation")
        basicAnimation.values = [0,0.3*force,-0.3*force,0.3*force,0]
        basicAnimation.keyTimes = [0.0,0.2,0.4,0.6,0.8,1.0]
        basicAnimation.duration = CFTimeInterval(animationDuration)
        basicAnimation.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
        self.viewLayer.addAnimation(basicAnimation, forKey: "wooble")
        if iswooblewithmove{
            switch woobleDirection {
            case .WoobleHorizontal:
                shake(shakeDirection:AnimationType.ShakeHorizontal)
            case .WoobleVertical :
                shake(shakeDirection: AnimationType.ShakeVertical)
            default:
                break
            }
        }
        
         performAnimation({})
    }
    //MARK:Morph
    /**
     Morphing effect on View Layer
     - parameter morphDirection : AnimationType reference
     - returns : No value is return
     */
    public func morphingEffect(morphDirection:AnimationType,animationRepeatcount:Float = 1,animationDuration:CGFloat = 0.5,force:CGFloat = 1,animationDelay:CGFloat = 0.5){
        
        switch morphDirection {
        case .Morph:
            let morphX = CAKeyframeAnimation()
            morphX.keyPath = "transform.scale.x"
            morphX.values = [1, 1.3*force, 0.7, 1.3*force, 1]
            morphX.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
            //morphX.timingFunction = getTimingFunction(animationCurve)
            morphX.duration = CFTimeInterval(animationDuration)
            morphX.repeatCount = animationRepeatcount
            morphX.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
            self.viewLayer.addAnimation(morphX, forKey: "morphX")
            
            let morphY = CAKeyframeAnimation()
            morphY.keyPath = "transform.scale.y"
            morphY.values = [1, 0.7, 1.3*force, 0.7, 1]
            morphY.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
            // morphY.timingFunction = getTimingFunction(curve)
            morphY.duration = CFTimeInterval(animationDuration)
            morphY.repeatCount = animationRepeatcount
            morphY.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
            self.viewLayer.addAnimation(morphY, forKey: "morphY")
        case .MorphX :
            let morphX = CAKeyframeAnimation()
            morphX.keyPath = "transform.scale.x"
            morphX.values = [1, 1.3*force, 0.7, 1.3*force, 1]
            morphX.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
            //morphX.timingFunction = getTimingFunction(animationCurve)
            morphX.duration = CFTimeInterval(animationDuration)
            morphX.repeatCount = animationRepeatcount
            morphX.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
            self.viewLayer.addAnimation(morphX, forKey: "morphX")
        case .MorphY :
            let morphX = CAKeyframeAnimation()
            morphX.keyPath = "transform.scale.y"
            morphX.values = [1, 1.3*force, 0.7, 1.3*force, 1]
            morphX.keyTimes = [0, 0.2, 0.4, 0.6, 0.8, 1]
            //morphX.timingFunction = getTimingFunction(animationCurve)
            morphX.duration = CFTimeInterval(animationDuration)
            morphX.repeatCount = animationRepeatcount
            morphX.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
            self.viewLayer.addAnimation(morphX, forKey: "morphY")
        default:
            break
        }
         performAnimation({})
    }
    //MARK:Flip Views about axis
    /**
     Flip View according to axis
     - parameter flipaxis: String reference
     - returns : No value is returned
     */
    public func flipViewAboutAxis(flipaxis flipaxis:AnimationType){
        self.scalingfactorX = 1
        self.scalingfactorY = 1
        var perspective = CATransform3DIdentity
        perspective.m34 = -1.0 / self.viewLayer.frame.size.width/2
        
        let animation = CABasicAnimation()
        animation.keyPath = "transform"
        animation.fromValue = NSValue(CATransform3D:
            CATransform3DMakeRotation(0, 0, 0, 0))
        
        
        if flipaxis == AnimationType.FlipY{
            animation.toValue = NSValue(CATransform3D:
                CATransform3DConcat(perspective,CATransform3DMakeRotation(CGFloat(M_PI), 1, 0, 0)))
        }
        else if flipaxis == AnimationType.FlipX{
            animation.toValue = NSValue(CATransform3D:
                CATransform3DConcat(perspective,CATransform3DMakeRotation(CGFloat(M_PI), 0, 1, 0)))
        }
        else{
            animation.toValue = NSValue(CATransform3D:
                CATransform3DConcat(perspective,CATransform3DMakeRotation(CGFloat(M_PI), 0, 0, 1)))
        }
        animation.duration = CFTimeInterval(animationDuration)
        animation.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
        //animation.timingFunction = getTimingFunction(curve)
        self.viewLayer.addAnimation(animation, forKey: "3d")
         performAnimation({})
    }

    private func performAnimation(completion: () -> ()){
        if animateFrom {
            let translate = CGAffineTransformMakeTranslation(self.xcordinate, self.ycordinate)
            let scale = CGAffineTransformMakeScale(self.scalingfactorX, self.scalingfactorY)
            let rotate = CGAffineTransformMakeRotation(self.rotationAngle)
            let translateAndScale = CGAffineTransformConcat(translate, scale)
            self.affineTransform = CGAffineTransformConcat(rotate, translateAndScale)
            //self.viewObject.transform = self.affineTransform
            //self.opacity  = 1
            //self.viewLayer.setAffineTransform(self.affineTransform)
        }
        // self.view.layer.setAffineTransform(self.affinetransform)
        UIView.animateWithDuration( NSTimeInterval(animationDuration),
                                    delay: NSTimeInterval(animationDelay),
                                    usingSpringWithDamping: animationDampingFactor,
                                    initialSpringVelocity: animationVelocity,
                                    options: [getAnimationOptions(animationCurve), UIViewAnimationOptions.AllowUserInteraction],
                                    animations: { [weak self] in
                                        if let _self = self
                                        {
                                            if _self.animateFrom {
                                                _self.affineTransform = CGAffineTransformIdentity
                                                //_self.opacity = 1
                                            }
                                            else {
                                                let translate = CGAffineTransformMakeTranslation(_self.xcordinate, _self.ycordinate)
                                                let scale = CGAffineTransformMakeScale(_self.scalingfactorX, _self.scalingfactorY)
                                                let rotate = CGAffineTransformMakeRotation(_self.rotationAngle)
                                                let translateAndScale = CGAffineTransformConcat(translate, scale)
                                                _self.affineTransform = CGAffineTransformConcat(rotate, translateAndScale)
                                                
                                                // _self.opacity = 1
                                            }
                                            
                                        }
                                        
            }, completion: { _ in
                
                completion()
                
                
        })
        
        
        
    }
    //MARK:Fade in out effect
    private func fadoutinOrfadeInout(fromValue fromValue:CGFloat,toValue:CGFloat){
        let basicAnimantion = CABasicAnimation(keyPath: "opacity")
        basicAnimantion.fromValue = fromValue
        basicAnimantion.toValue = toValue
        basicAnimantion.autoreverses = true
        basicAnimantion.duration = CFTimeInterval(animationDuration)
        basicAnimantion.beginTime = CACurrentMediaTime() + CFTimeInterval(animationDelay)
        basicAnimantion.autoreverses = true
        basicAnimantion.repeatCount = animationRepeatCount
        self.viewLayer.addAnimation(basicAnimantion, forKey: "fadding")
         performAnimation({})
    }
    
    /**
     Get Animation Options using animation curve property
     - parameter curve : String reference
     - returns : No value is return
     */
    func getAnimationOptions(curve:AnimationCurve) -> UIViewAnimationOptions {
        
        switch curve {
        case .EaseIn: return UIViewAnimationOptions.CurveEaseIn
        case .EaseOut: return UIViewAnimationOptions.CurveEaseOut
        case .EaseInOut: return UIViewAnimationOptions.CurveEaseInOut
        default: break
        }
        
        return UIViewAnimationOptions.CurveLinear
    }
    
    
    
}