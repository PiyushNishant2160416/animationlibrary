//
//  AnimateView.swift
//  ViewAnimation
//
//  Created by Piyush on 09/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
import UIKit
public class AnimateView:NSObject{
    
    // Reference for AnimationProtocol
    private unowned var view : AnimationProtocol
    private var viewObject : UIView
    //Constructor to initialize view
    init(view:AnimationProtocol,viewObject:UIView) {
        self.view = view
        self.viewObject = viewObject
    }
    
    /******      DECLARE STORED TYPE PROPERTIES  AND GET AND SET VALUE FROM UIView CLASS    **********/
    
//    typealias chainAnimationClosure  = () -> ()
//    var chainAnimationClosureArray:[chainAnimationClosure] = [chainAnimationClosure]()
//    var chainAnimationArray = [AnyObject]()
    
    var chainAnimationArray:[ChainAnimationModal] = [ChainAnimationModal]()
    
    
//    private var viewObject : UIView{
//        get{
//            return self.view.viewObject
//        }
//    }
    
    
    private var autoStart : Bool{
        get{
            return self.view.autoStart
        }
        set{
            self.view.autoStart = newValue
        }
    }
    
    private var autoHide : Bool{
        get{
            return self.view.autoHide
        }
        set{
            self.view.autoHide = newValue
        }
    }
    private var isremoveFromSuperView:Bool{
        get{
            return self.view.isremoveFromSuperView
        }
        set{
            self.view.isremoveFromSuperView = newValue
        }
    }
    private var animationType:String{
        get{
            return self.view.animationType
        }
        set{
            self.view.animationType = newValue
        }
    }
    private var force:CGFloat{
        get{
            return self.view.force
        }
        set{
            self.view.force = newValue
        }
    }
    private var animationDelay:CGFloat{
        get{
            return self.view.animationDelay
        }
        set{
            self.view.animationDelay = newValue
        }
    }
    private var animationDuration:CGFloat{
        get{
            return self.view.animationDuration
        }
        set{
            self.view.animationDuration = newValue
        }
    }
    
    private var animationVelocity:CGFloat{
        get{
            return self.view.animationVelocity
        }
        set{
            self.view.animationVelocity = newValue
        }
    }
    private var animationDampingFactor:CGFloat{
        get{
            return self.view.animationDampingFactor
        }
        set{
            self.view.animationDampingFactor = newValue
        }
    }
    private var animationRepeatcount:Float{
        get{
            return self.view.animationRepeatcount
        }
        set{
            self.view.animationRepeatcount = newValue
        }
    }
    private var Xcordinate:CGFloat{
        get{
            return self.view.Xcordinate
        }
        set{
            self.view.Xcordinate = newValue
        }
    }
    private var Ycordinate:CGFloat{
        get{
            return self.view.Ycordinate
        }
        set{
            self.view.Ycordinate = newValue
        }
    }
    private var scalingFactorX:CGFloat{
        get{
            return self.view.scalingFactorX
        }
        set{
            self.view.scalingFactorX = newValue
        }
    }
    private var scalingFactorY:CGFloat{
        get{
            return self.view.scalingFactorY
        }
        set{
            self.view.scalingFactorY = newValue
        }
    }
    private var rotationAngle:CGFloat{
        get{
            return self.view.rotationAngle
        }
        set{
            self.view.rotationAngle = newValue
        }
    }
    private var animateFrom:Bool{
        get{
            return self.view.animateFrom
        }
        set{
            self.view.animateFrom = newValue
        }
    }
    private var layer:CALayer{
        get{
            return self.view.layer
        }
        
    }
    private var affinetransform:CGAffineTransform{
        get{
            return self.view.transform
        }
        set{
            self.view.transform = newValue
        }
    }
    private var opacity:CGFloat{
        get{
            return self.view.opacity
        }
        set{
            self.view.opacity = newValue
        }
    }
    private var alpha:CGFloat{
        get{
            return self.view.alpha
        }
        set{
            self.view.alpha = newValue
        }
    }
    private var animationCurve:String{
        get{
            return self.view.animationCurve
        }
        set{
            self.view.animationCurve = newValue
        }
    }
    
    /**
     Animate View according to animationType
     - parameter animationType : String reference
     - returns : No return value
    */
    func animate(animationType:String){
        
        
        if let animationType = AnimationType(rawValue: animationType){
            switch animationType {
            case .SlideRight,.SlideDown,.SlideUp,.SlideLeft:
                moveViewFromOnePointToOther(toPointX: Xcordinate, toPointY: Ycordinate, direction: animationType)
            case .TransitionCurlUp,.TransitionFromTop,.TransitionCurlDown,.TransitionFromBottom,.TransitionFlipFromLeft,.TransitionCrossDissolve,.TransitionFlipFromRight :
                viewWithTransitionAnimation(animationType: animationType)
            case .FadeIn,.FadeOut :
                fadeInAndFadeOut(animationType: animationType)
            case .AnimationChain :
                performChainAnimation()
            case .Shear :
                var affineTransform = CGAffineTransformIdentity
                affineTransform.c = Xcordinate
                affineTransform.b = Ycordinate
                self.viewObject.layer.setAffineTransform(affineTransform)
            case .RotationArroundX,.RotationArroundY,.RotationArroundZ :
                perform3DRotationOnView(animationType)
            case .ReflectedView :
//                let careplicatorLayer = CAReplicatorLayer(layer:self.viewObject.layer)
//                careplicatorLayer.instanceCount = 2
//                var transform3d = CATransform3DIdentity
//                let vertocalOffset : CGFloat = self.viewObject.bounds.size.height + 2
//                transform3d = CATransform3DTranslate(transform3d, 0, vertocalOffset, 0)
//                transform3d = CATransform3DScale(transform3d, 1, -1, 0)
//                careplicatorLayer.instanceTransform = transform3d
//                careplicatorLayer.instanceAlphaOffset = 0.6
//                careplicatorLayer.addSublayer(self.layer)
                let replicatorLayer = CAReplicatorLayer(layer: self.layer)
                replicatorLayer.frame = self.viewObject.bounds
                replicatorLayer.instanceCount = 2
                var transfortm3d = CATransform3DIdentity
                let Y = self.viewObject.bounds.size.height + 100
                transfortm3d = CATransform3DTranslate(transfortm3d, 0, Y, 0)
                transfortm3d = CATransform3DScale(transfortm3d, 1, -1, 0)
                replicatorLayer.instanceTransform = transfortm3d
                replicatorLayer.opacity = 0.5
               // self.viewObject.layer.addSublayer(replicatorLayer)
            default:
                break
            }
        }
    }
    /**
     Perform Slide functionality on View
     - parameter toPoint,direction : toPoint is a CGFloat type reference which detrmine to which point view is slidede.
          direction is the ANimationType Reference which determine which direction view will move
     - returns : No value is return
    */
    private func moveViewFromOnePointToOther(toPointX toPointX:CGFloat,toPointY:CGFloat,direction:AnimationType){
        UIView.animateWithDuration(NSTimeInterval(animationDuration), delay: NSTimeInterval(animationDelay), usingSpringWithDamping: animationDampingFactor, initialSpringVelocity: animationVelocity, options: [getAnimationOptions(curve: animationCurve)], animations: {
            switch direction{
            case .SlideRight :
                self.viewObject.center.x += self.Xcordinate
            case .SlideLeft :
                self.viewObject.center.x -= self.Xcordinate
            case .SlideUp :
                self.viewObject.center.y -= self.Ycordinate
            case .SlideDown :
                self.viewObject.center.y += self.Ycordinate
            default :
                break
            }
            
            }, completion:{ _ in
                if self.isremoveFromSuperView {
                    self.viewObject.removeFromSuperview()
                }
                if self.autoHide {
                    self.viewObject.hidden = true
                }
        
        })
        
    }
    /**
     View Transition Animation
     - parameter animationType : AnimationType reference 
     - returns : No value is return
    */
    private func viewWithTransitionAnimation(animationType animationType:AnimationType){
        
        UIView.transitionWithView(self.viewObject, duration: NSTimeInterval(animationDuration), options:[getAnimationOptions(animationType: animationType),getAnimationOptions(curve: animationCurve)], animations: {
            }, completion: {
                _ in
                if self.isremoveFromSuperView {
                    self.viewObject.removeFromSuperview()
                }
                if self.autoHide {
                    self.viewObject.hidden = true
                }

                
        })
        
    }
    /**
     Fade In and Fade Out effect 
     - parameter animationType : AnimationType reference
     - returns : No value is return
    */
    private func fadeInAndFadeOut(animationType animationType:AnimationType){
        var viewObjectalpha:CGFloat = 0
        switch animationType {
        case .FadeOut:
            viewObjectalpha = 0
        case .FadeIn :
            viewObjectalpha = 1
        default:
            break
        }
        UIView.animateWithDuration(NSTimeInterval(animationDuration), delay: NSTimeInterval(animationDelay), options:[getAnimationOptions(curve:animationCurve),getAnimationOptions(animationType: animationType)], animations: {
            self.viewObject.alpha = viewObjectalpha
            
            }, completion: {
                _ in
                if self.isremoveFromSuperView {
                    self.viewObject.removeFromSuperview()
                }
                if self.autoHide {
                    self.viewObject.hidden = true
                }
        })
    }
    /**
     Chain Animation using Keyframe ANimation
     - parameter  No parameter :
     - returns : No value is return
    */
    private func performChainAnimation(){
        if self.chainAnimationArray.count > 0{
            
          
            
            UIView.animateKeyframesWithDuration(NSTimeInterval(animationDuration), delay: NSTimeInterval(animationDelay), options: [], animations: {
                for (_,chainanimation) in self.chainAnimationArray.enumerate(){
                    let startTime:CGFloat = chainanimation.animationStartTime
                    let relativeDuration :Double = chainanimation.animationrelativeDuration
                    let animationClosure  = chainanimation.chainAnimationClosure                    
                    UIView.addKeyframeWithRelativeStartTime(NSTimeInterval(startTime), relativeDuration: relativeDuration){
                        animationClosure()
                    }
                }
                
                
                }, completion: {
                _ in
                    if self.isremoveFromSuperView {
                        self.viewObject.removeFromSuperview()
                    }
                    if self.autoHide {
                        self.viewObject.hidden = true
                    }
                    
            })
        }
        
    }
    
    /**
     set chain animation details with closure details 
     - parameter chainAnimationArray,closureArray : Array of AnyObject,closure array stores the self execution code
     - returns : No value is return
    */
    public func setChainAnimationDetails(chainAnimationarray:[ChainAnimationModal]){
//        self.chainAnimationArray = chainAnimationArray
//        self.chainAnimationClosureArray = closureArray
        //chainAnimationArray = [ChainAnimationModal]()
        self.chainAnimationArray = chainAnimationarray
        
    }
    
    /**
     Rotate View about X,Y and Z axis by using rotation angle
    */
    private func perform3DRotationOnView(animationType:AnimationType){
        var transform3d = CATransform3DIdentity
        transform3d.m34 = -1/500
        switch animationType{
            
        case .RotationArroundY:
            transform3d = CATransform3DRotate(transform3d, self.rotationAngle, 0, 1, 0)
        case .RotationArroundX :
            transform3d = CATransform3DRotate(transform3d, self.rotationAngle, 1, 0, 0)
        case .RotationArroundZ :
            transform3d = CATransform3DRotate(transform3d, self.rotationAngle, 0, 0, 1)
        default:
            break
        }
        self.viewObject.layer.transform = transform3d
    }
    
    
    /**
     Get UIView Animation Option
     - parameter animationType : AnimationType reference used to get Animation Option
     - returns : No value is return
    */
    
    private func getAnimationOptions(animationType animationType:AnimationType) -> UIViewAnimationOptions{
        var transitionOption : UIViewAnimationOptions = UIViewAnimationOptions.TransitionNone
        switch animationType{
        case .TransitionFlipFromRight:
            transitionOption = UIViewAnimationOptions.TransitionFlipFromRight
        case .TransitionFlipFromLeft :
            transitionOption = UIViewAnimationOptions.TransitionFlipFromLeft
        case .TransitionFromBottom :
            transitionOption = UIViewAnimationOptions.TransitionFlipFromBottom
        case .TransitionFromTop :
            transitionOption = UIViewAnimationOptions.TransitionFlipFromTop
        case .TransitionCurlUp :
            transitionOption = UIViewAnimationOptions.TransitionCurlUp
        case .TransitionCurlDown :
            transitionOption = UIViewAnimationOptions.TransitionCurlDown
        default :
            transitionOption  = UIViewAnimationOptions.TransitionNone
        }
        return transitionOption

    }
    /**
     Get Animation Options using animation curve property
     - parameter curve : String reference
     - returns : No value is return
     */
    func getAnimationOptions(curve curve: String) -> UIViewAnimationOptions {
        if let curve = AnimationCurve(rawValue: curve) {
            switch curve {
            case .EaseIn: return UIViewAnimationOptions.CurveEaseIn
            case .EaseOut: return UIViewAnimationOptions.CurveEaseOut
            case .EaseInOut: return UIViewAnimationOptions.CurveEaseInOut
            default: break
            }
        }
        return UIViewAnimationOptions.CurveLinear
    }

    
}
 