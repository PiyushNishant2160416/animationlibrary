//
//  AnimationProtocol.swift
//  ViewAnimation
//
//  Created by Piyush on 06/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
import UIKit
 
 @objc protocol AnimationProtocol {
    
    var autoStart : Bool {get set}
    var autoHide : Bool {get set}
    var isremoveFromSuperView : Bool {get set}
    var animationType : String {get set}
    var force : CGFloat {get set}
    var animationDelay : CGFloat {get set}
    var animationDuration : CGFloat {get set}
    var animationVelocity : CGFloat {get set}
    var animationDampingFactor:CGFloat{get set}
    var animationRepeatcount : Float {get set}
    var Xcordinate : CGFloat {get set}
    var Ycordinate : CGFloat {get set}
    var scalingFactorX : CGFloat {get set}
    var scalingFactorY : CGFloat {get set}
    var rotationAngle : CGFloat{get set}
    var animateFrom : Bool {get set}
    var layer : CALayer {get}
    var transform : CGAffineTransform {get set}
    var opacity : CGFloat  {get set}
    var alpha : CGFloat {get set}
    var animationCurve:String{get set}
    //Pulse Animation Properties
//    optional var numberOfPulse:Int{get set}
//    optional var backgroundColor:CGColor{get set}
//    optional var pulseRadius:CGFloat{get set}
    
   func animate()
}
