//
//  AnimatableCustomView.swift
//  ViewAnimation
//
//  Created by Piyush on 06/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import UIKit

class AnimatableCustomView: UIView,AnimationProtocol {

    //ChainAnimation FUnctionality
    var chainAnimationModalObjectArray = [ChainAnimationModal]()
    
    
    
    
    var isFadeout = false
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
     
     @IBInspectable var autoStart: Bool = false
     var autoHide: Bool = false
     var isremoveFromSuperView = false
     var animationType: String = AnimationType.ReflectedView.rawValue
     var force: CGFloat = 1
     var animationDelay: CGFloat = 0.5
     var animationDuration: CGFloat = 1
     var animationDampingFactor: CGFloat = 0.7
     var animationVelocity: CGFloat = 0.7
     var animationRepeatcount: Float = 1
     var Xcordinate: CGFloat = 0
     var Ycordinate: CGFloat = 1
     var scalingFactorX: CGFloat = 1
     var scalingFactorY: CGFloat = 1
     var rotationAngle: CGFloat = CGFloat(M_PI_4)
     var animationCurve : String = AnimationCurve.EaseInOut.rawValue
    
    //var viewObject :UIView = UIView()
    
    
    internal var transformation  = CGAffineTransform()
   // @IBInspectable internal var curve: String = ""
    internal var opacity: CGFloat = 1
    internal var animateFrom: Bool = false

    //lazy private var AnimateObject : Animate = Animate(view: self)
    lazy private var animateViewObject : AnimateView = AnimateView(view: self, viewObject: self)
    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        
        
        
       // self.AnimateObject.awakeFromNib()
       // viewObject = self
        let chainAnimationModalObject1 = ChainAnimationModal()
        
        chainAnimationModalObject1.animationStartTime = 0.0
        chainAnimationModalObject1.animationrelativeDuration = 0.4
        chainAnimationModalObject1.chainAnimationClosure = {
            self.center.x += 100
        }
        
        let chainAnimationModalObject2 = ChainAnimationModal()
        
        chainAnimationModalObject2.animationStartTime = 0.42
        chainAnimationModalObject2.animationrelativeDuration = 0.3
        chainAnimationModalObject2.chainAnimationClosure = {
            self.center.y += 100
        }

        
        chainAnimationModalObjectArray.append(chainAnimationModalObject1)
        chainAnimationModalObjectArray.append(chainAnimationModalObject2)
        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    
    
    func animate() {
       // animateViewObject.setChainAnimationDetails(chainAnimationModalObjectArray)
        //animateViewObject.animate(animationType)
        //AnimateObject.animate()
        
        let pulse = PulseAnimation()
        self.layer.addSublayer(pulse)
        pulse.position = self.center
        pulse.numberOfpulse = 5
        pulse.animationDuration = 5
        pulse.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 1).CGColor
        pulse.startPulse()
//        let pulsator = Pulsator()
//        
//        self.layer.addSublayer(pulsator)
//        //self.superview?.layer.insertSublayer(pulsator, below: sourceView.layer)
//        pulsator.position = self.center
//        
//        // Customizations
//        pulsator.numPulse = 5
//        pulsator.radius = 200
//        pulsator.animationDuration = 5
//        pulsator.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 1).CGColor
//        
//        pulsator.start()
        
        
    }
    
    
}
