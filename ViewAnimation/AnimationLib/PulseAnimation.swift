//
//  PulseAnimation.swift
//  ViewAnimation
//
//  Created by Piyush on 12/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore.QuartzCore
public class PulseAnimation : CAReplicatorLayer{
    
    private var pulseLayer = CALayer()
    private var animationgroup : CAAnimationGroup!
    private var pulseOpacity : CGFloat = 0.45
    
    
    
    //var pulseRepeatCount : Float
    //var pulseBackground:CGColor
    var pulseDelay :Double
    
    
   // public var pulseRadius : CGFloat = 60
    public var pulsestartRadius : CGFloat = 0
    public var animationDuration :NSTimeInterval = 5{
        didSet{
            updateInstanceDelay()
        }
    }
    public var halfOpacityTime :CGFloat = 0.2
    public var pulseInterval : NSTimeInterval = 0
    public var timingFunction: CAMediaTimingFunction? = CAMediaTimingFunction(name: kCAMediaTimingFunctionDefault) {
        didSet {
            if let animationGroup = animationgroup {
                animationGroup.timingFunction = timingFunction
            }
        }
    }
    public var numberOfpulse : Int = 5 {
        didSet{
            if numberOfpulse < 1{
                numberOfpulse = 1
            }
            instanceCount = numberOfpulse
            updateInstanceDelay()
        }
        
    }
    
    override public var backgroundColor: CGColor?{
        didSet{
            pulseLayer.backgroundColor = backgroundColor
            let oldAlpha = pulseOpacity
            pulseOpacity = CGColorGetAlpha(backgroundColor)
            if animationgroup != nil && pulseOpacity != oldAlpha {
                
                recreate()
            }
        }

  }
    public var pulseRadius : CGFloat = 60{
        didSet{
            upDatePulse()
        }
    }

    override public var repeatCount: Float{
        didSet{
            if let animationGroup = animationgroup{
                animationGroup.repeatCount = repeatCount
            }
        }
    }
    
    public var autoRemove = false
    
    //Initializer
    override init(){
        
        pulseDelay = 1
        super.init()
        self.repeatCount  = MAXFLOAT
        self.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 0.45).CGColor
        
    }
    
    override public init(layer: AnyObject) {
        
        pulseDelay = 1
        super.init(layer: layer)
        repeatCount  = MAXFLOAT
        backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 0.45).CGColor
    }
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    /**
     Start pulse
     - parameter No parameter:
     - returns : No value is return
    */
    func startPulse(){
        
        pulseConfiguration()
        setAnimationGroup()
        
        pulseLayer.addAnimation(animationgroup, forKey: "piyush")
    }
    /**
     Configure Animation Group and add animations to animation group
     - parameter No parameter:
     - returns : No value is return
    */
    private func setAnimationGroup(){
        //Animation Group
        let pulseScaleAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
        pulseScaleAnimation.fromValue = pulsestartRadius
        pulseScaleAnimation.toValue = 1.0
        pulseScaleAnimation.duration = animationDuration
        
        //Opacity Animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        opacityAnimation.duration = animationDuration
        opacityAnimation.values = [pulseOpacity,pulseOpacity*0.5,0]
        opacityAnimation.keyTimes = [0,halfOpacityTime,1]
        
        
        animationgroup = CAAnimationGroup()
        animationgroup.animations = [pulseScaleAnimation,opacityAnimation]
        animationgroup.duration = animationDuration + pulseInterval
        animationgroup.repeatCount = repeatCount
        animationgroup.timingFunction = timingFunction

    }
    /**
     Configure Pulse
     - parameter No parameter :
     - returns : No value is return
    */
    private func pulseConfiguration(){
        pulseLayer.backgroundColor = backgroundColor
        pulseLayer.contentsScale = UIScreen.mainScreen().scale
        pulseLayer.opacity = 0
        self.addSublayer(pulseLayer)
        upDatePulse()
    }
    
    /**
     Update Instance Delay whenever number of pulse or Animation duration is changed
     - parameter No parameter :
     - returns : No value is return
    */
    private func updateInstanceDelay(){
        guard self.numberOfpulse >= 1 else {
            fatalError()
        }
        instanceDelay = (animationDuration + pulseInterval) / Double(numberOfpulse)
    }
    /**
     Update pulse whenever radious or background is changed
     - parameter No parameter :
     - returns : No value is return
    */
    private func upDatePulse(){
        let diameter: CGFloat = pulseRadius * 2
        pulseLayer.bounds = CGRect(
            origin: CGPointZero,
            size: CGSizeMake(diameter, diameter))
        pulseLayer.cornerRadius = pulseRadius
        
        if backgroundColor == nil {
            backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 0.45).CGColor
        }
        
        pulseLayer.backgroundColor = backgroundColor
    }
    /**
     Create the pulse
     - parameter No parameter:
     - returns : No value is return
    */
    private func recreate() {
        
        stop()
        let when = dispatch_time(DISPATCH_TIME_NOW, Int64(0.2 * double_t(NSEC_PER_SEC)))
        dispatch_after(when, dispatch_get_main_queue()) { () -> Void in
            self.startPulse()
        }
    }
    /**
     Stop Animation
     - parameter  No parameter :
     - returns : No value is return
    */
    public func stop() {
        pulseLayer.removeAllAnimations()
        animationgroup = nil
    }

}