//
//  AnimationType.swift
//  ViewAnimation
//
//  Created by Piyush on 06/05/16.
//  Copyright © 2016 Piyush. All rights reserved.
//

import Foundation
public enum AnimationType : String{
    /***************************        LAYER ANIMATION          **************************************/
    case SlideLeft = "slideleft"
    case SlideRight = "slideright"
    case SlideUp = "slideup"
    case SlideDown = "slidedown"
    case SqueezeLeft = "squeezeleft"
    case SqueezeRight = "squeezeright"
    case SqueezeUp = "squeezeup"
    case SqueezeDown = "squeezedown"
    case FadeIn = "fadein"
    case FadeOut = "fadeout"
    case FadeInLeft = "fadeinleft"
    case FadeInRight = "fadeinright"
    case FadeInDown = "fadeindown"
    case FadeInUp = "fadeinup"
    case FadeInOut = "fadeinout"
    case FadeOutIn = "fadeoutin"
    case ZoomIn = "zoomin"
    case ZoomOut = "zoomout"
    case Fall = "fall"
    case ShakeVertical = "shakevertical"
    case ShakeHorizontal = "shakehorizontal"
    case Pop = "pop"
    case FlipX = "flipx"
    case FlipY = "flipy"
    case FlipZ = "flipz"
    case Flash = "flash"
    case Wooble = "wooble"
    case WoobleHorizontal = "wooblehorizontal"
    case WoobleVertical = "wooblevertical"
    case RotationWithShake  = "rotationwithshake"
    case Morph = "morph"
    case MorphX = "morphx"
    case MorphY = "morphy"
    case Swing = "swing"
   // case Shear = "shear"
    
    /**************************     VIEW ANIMATION TYPE            *********************************************/
    case TransitionFlipFromLeft  = "transitionflipfromleft"
    case TransitionFlipFromRight = "transitionflipfromright"
    case TransitionCurlUp = "transitioncurlup"
    case TransitionCurlDown = "transitioncurldown"
    case TransitionCrossDissolve = "transitioncrossdissolve"
    case TransitionFromTop = "transitionfromtop"
    case TransitionFromBottom = "transitionfrombottom"
    case AnimationChain = "animationchain"
    case Shear = "shear" //Use Xcordinate and YCordinate value range 0 to 1 and according to shearing angle
    //These Rotation based on X,Y and Z axis according to rotation angle
    case RotationArroundX = "rotationarroundx"
    case RotationArroundY = "rotationarroundy"
    case RotationArroundZ  = "rotationarroundz"
    //Create Reflection view of main view
    case ReflectedView  = "reflectedview"
   
}
public enum AnimationCurve: String {
    case EaseIn
    case EaseOut
    case EaseInOut
    case Linear
}

